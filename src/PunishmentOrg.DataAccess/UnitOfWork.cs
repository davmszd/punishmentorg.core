﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PunishmentOrg.DataAccess
{
    public class UnitOfWork : Domain.Interface.IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        Domain.Interface.Anu.BaseInfo.OrganizationChart.IUnitRepository _Unit;
        public Domain.Interface.Anu.BaseInfo.OrganizationChart.IUnitRepository Unit 
        {
            get 
            {
                if (_Unit is not null)
                {
                    return _Unit;
                }

                return _Unit = new Repositories.Anu.BaseInfo.OrganizationChart.UnitRepository(_context);
            } 
        }

        Domain.Interface.Anu.BaseInfo.OrganizationChart.IGUnitTypeRepository _GUnitType;
        public Domain.Interface.Anu.BaseInfo.OrganizationChart.IGUnitTypeRepository GUnitType 
        {
            get
            {
                if (_GUnitType is not null)
                {
                    return _GUnitType;
                }

                return _GUnitType = new Repositories.Anu.BaseInfo.OrganizationChart.GUnitTypeRepository(_context);
            }
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

    }
}
