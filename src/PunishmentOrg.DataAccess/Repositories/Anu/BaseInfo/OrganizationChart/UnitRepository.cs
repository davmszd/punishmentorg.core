﻿using Microsoft.EntityFrameworkCore;
using PunishmentOrg.Domain.Entities.Anu.BaseInfo.OrganizationChart;
using PunishmentOrg.Domain.Interface;
using PunishmentOrg.Domain.Interface.Anu.BaseInfo.OrganizationChart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PunishmentOrg.DataAccess.Repositories.Anu.BaseInfo.OrganizationChart
{
    public class UnitRepository : GenericRepository<Unit>, IUnitRepository
    {
        public UnitRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Unit>> GetUnitWithParentUnitList(string Id)
        {
            var q = _context.Unit.Include(x => x.TheGUnitType).Where(x => x.Id == Id).ToQueryString();
            //return await _context.Unit.Include(x => x.TheGUnitType).Where(x => x.Id == Id).ToListAsync();

            var q1 = _context.Unit.FromSqlRaw(@"select id,code,unitname from unit u where u.id = '" + Id + "'").ToQueryString();
            var q2 = _context.Unit.FromSqlInterpolated($"select id,code,unitname from unit u where u.id = {Id}").ToQueryString();

            return await _context.Unit.FromSqlRaw("select id,code,unitname from unit u where u.id = '"+Id+"'").ToListAsync();





            //_context.Set<Unit>().FromSqlRaw("select * from ");


        }
    }
}
