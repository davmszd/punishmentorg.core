﻿using PunishmentOrg.Domain.Entities.Anu.BaseInfo.OrganizationChart;

namespace PunishmentOrg.Domain.Interface.Anu.BaseInfo.OrganizationChart
{
    public interface IGUnitTypeRepository : IGenericRepository<GUnitType>
    {
    }
}
